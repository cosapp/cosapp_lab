=================
Using CoSApp Lab
=================

The main function of CoSApp Lab is to create and deploy interactive dashboards from CoSApp system. The creation part is handled by a JupyterLab extension and  CoSApp Lab provides a CLI tool for deployment.

The layout of dashboard can be customized dynamically with predefined widgets, which are made to work directly with CoSApp system so the creation of dashboard can be done entirely without coding.

The following sections cover two aspects of CoSApp Lab.

.. toctree::
   :maxdepth: 3

   lab/extension

   app/standalone
