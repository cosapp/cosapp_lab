stages:
  - test
  - test_build
  - test_doc
  - deploy

variables:
  COSAPPLAB_DEP: cosapp "jupyterlab>=4,<5"  pytest "ipywidgets>=8,<9" nbconvert  nodejs=20 cookiecutter "pythonocc-core>=7.6" click requests "jupyter_client>=8" jinja2 "cosapp::pyoccad"
  USER: 'cosapp_test'
  GIT_SUBMODULE_STRATEGY: recursive

# Test script for all OS
.test_script:
  script:
    - python -m pytest
    - npm run test

.coverage_script:
  script:
    - mamba install -y coverage
    - coverage erase
    - coverage run -m pytest -ra
    - coverage report
    - coverage html -d coverage/
    - npm run test

# default linux environment parameters
.linux_common_env:
  image: 'condaforge/mambaforge:latest'
  before_script:
    - conda init bash
    - source $HOME/.bashrc
    - mamba create -y -n cosapp_lab python=$PYTHON_VERSION
    - source activate cosapp_lab
    - mamba install -y $COSAPPLAB_DEP
    - npm install --legacy-peer-deps
    - npm run build:prod
    - pip install . --trusted-host=files.pythonhosted.org

# default linux job parameters
.linux_env:
  extends:
    - '.linux_common_env'
  tags:
    # - gitlab-org
    - cosapp_runner
  stage: test

# Deployment env (runs on public runner)
.deploy_env:
  extends:
    - '.linux_common_env'
  stage: deploy

linux_py39:
  extends:
    - '.linux_env'
    - '.coverage_script'
  variables:
    PYTHON_VERSION: '3.9'

  artifacts:
    paths:
      - coverage/
    expire_in: 2 weeks

  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'

linux_py39_mr:
  extends:
    - '.linux_env'
    - '.coverage_script'
  variables:
    PYTHON_VERSION: '3.9'

  artifacts:
    paths:
      - coverage/
    expire_in: 2 weeks

  rules:
    - if: $CI_MERGE_REQUEST_ID

build_pkg:
  stage: test_build
  extends:
    - '.linux_env'
  variables:
    JOB_DEP: conda-build conda-verify
    PYTHON_VERSION: '3.11'
  script:
    - mamba install -y $JOB_DEP
    - python -m pip install build
    - python -m build .
    - conda build -c conda-forge --no-test --output-folder ./dist conda.recipe
  artifacts:
    paths:
      - dist/
  only:
    - master
    - merge_requests

build_doc:
  extends:
    - '.linux_env'
  stage: test_doc
  variables:
    PYTHON_VERSION: '3.11'
  script:
    - mamba install -y "sphinx<7.3" jinja2 sphinx-copybutton sphinx-mdinclude sphinxcontrib-mermaid
    - cd docs
    - sphinx-build -b html -d _build/doctrees . _build
  only:
    - merge_requests

deploy:
  extends:
    - '.deploy_env'
  variables:
    JOB_DEP: setuptools wheel twine packaging anaconda-client conda-build conda-verify
    TWINE_USERNAME: __token__
    CONDA_USERNAME: cosapp
    TWINE_PASSWORD: $PYPI
    CONDA_UPLOAD_TOKEN: $CONDA_UPLOAD_TOKEN
    PYTHON_VERSION: '3.11'
  script:
    - python -m pip install --upgrade pip
    - mamba install -y $JOB_DEP
    - python -m pip install build
    - python -m build .
    - twine upload --skip-existing dist/*.tar.gz
    - twine upload --skip-existing dist/*.whl
    - conda build -c conda-forge --no-test --output-folder ./dist conda.recipe
    - anaconda -t $CONDA_UPLOAD_TOKEN upload -u $CONDA_USERNAME ./dist/noarch/cosapp_lab*

  artifacts:
    paths:
      - dist/
  only:
    - tags

pages:
  extends:
    - '.deploy_env'
  variables:
    PYTHON_VERSION: '3.11'
  script:
    - mamba install -y "sphinx<7.3" jinja2 sphinx-copybutton sphinx-mdinclude sphinxcontrib-mermaid
    - cd docs
    - sphinx-build -b html -d _build/doctrees . _build
    - mv _build/ ../public
  artifacts:
    paths:
      - public
  only:
    - master
